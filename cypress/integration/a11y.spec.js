/* eslint-disable no-undef */
/// <reference types="Cypress" />

describe("Home", () => {
  it("Has no detectable a11y violations on load (custom configuration)", () => {
    // use fixtures or cy.route to intercept requests instead of wait
    // https://docs.cypress.io/api/commands/fixture.html#Syntax
    // https://docs.cypress.io/api/commands/route.html
    cy.wait(2000);
    cy.checkA11y();
  });
});

describe("User", () => {
  it("Has no a11y violations on the login page", () => {
    cy.get(".nav-link")
      .contains("Sign in")
      .click()
      .checkA11y();
  });
});
