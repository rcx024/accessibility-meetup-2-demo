FROM node:10 as build
COPY . /usr/src/app
WORKDIR /usr/src/app
RUN yarn && yarn build

FROM nginx:1.17
COPY --from=build /usr/src/app/dist /usr/share/nginx/html
EXPOSE 80
