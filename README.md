[![RealWorld Frontend](https://img.shields.io/badge/realworld-frontend-%23783578.svg)](http://realworld.io)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

# Countinous Accessibility demo

[Prezentáció letöltése](https://drive.google.com/open?id=1ZiwyZgz9UG0CP4nOnmGwN1ERydH9NUOp)

> Az alábbi repository a [RealWorld](https://github.com/gothinkster/realworld) Vue.js példáját veszi alapul.

``` bash
# függőségek telepítése
> yarn
# projekt indítása
> yarn serve
```

Accessibility ellenőrzéseket tartalmazó end-to-end teszt futtatása:

``` bash
yarn test:a11y:dev
```

## Projekt felépítése

CI Konfiguráció

```
./gital-ci.yml
```

Cypress Konfiguráció

```
./cypress.json
```

Cypress file-ok

```js
./cypress/support/index.js // cypress-axe library-t itt inicializáljuk

./cypress/integration/a11y.spec.js // Accessibility ellenőrzéseket tartalmazó e2e tesztek
```

## Források

- [axe-core](https://github.com/dequelabs/axe-core)
- [axe config](https://www.deque.com/axe/axe-for-web/documentation/api-documentation/#api-name-axeconfigure)
- [cypress](https://www.cypress.io/)
- [cypress-axe](https://www.npmjs.com/package/cypress-axe)
- [gitlab ci](https://about.gitlab.com/product/continuous-integration/)
